# Copyright 2019-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=intel pn=${PN/intel-} tag=${PV} ] \
    cmake

SUMMARY="Intel Graphics Compute Runtime for OpenCL Driver"
DESCRIPTION="
The Intel Graphics Compute Runtime for OpenCL is an open source project to converge Intel's
development efforts on OpenCL compute stacks supporting the GEN graphics hardware architecture.
"
HOMEPAGE+=" https://01.org/compute-runtime"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    va [[ description = [ Enable the cl_intel_va_api_media_sharing OpenCL extension ] ]]
"

# NOTE: For more information about the need for the blocker:
# https://bugs.llvm.org/show_bug.cgi?id=30587
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=852746
DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        virtual/pkg-config
    build+run:
        dev-libs/intel-gmmlib[>=22.2.0]
        dev-util/intel-graphics-compiler[>=1.0.12149.1]
        va? ( x11-libs/libva[>=2.2.0] )
    run:
        !x11-dri/mesa[opencl] [[
            description = [ Due to a LLVM bug there can be only one LLVM based OpenCL implementation ]
            resolution = manual
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_WITH_L0:BOOL=FALSE
    -DCCACHE_ALLOWED:BOOL=FALSE
    -DNEO_DISABLE_BUILTINS_COMPILATION:BOOL=FALSE
    -DNEO_OCL_VERSION_MAJOR=$(ever major)
    -DNEO_OCL_VERSION_MINOR=$(ever range 2)
    -DNEO_SKIP_UNIT_TESTS:BOOL=TRUE
    -DNEO_VERSION_BUILD:STRING=$(ever range 3)
    -DSUPPORT_DG1:BOOL=TRUE
    -DUSE_ASAN:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    '!va DISABLE_LIBVA'
)

src_prepare() {
    cmake_src_prepare

    edo sed \
        -e 's: -Werror::g' \
        -i CMakeLists.txt
}

