# Copyright 2018-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever at_least 5.0.0 ; then
    # With 5.0.0 there's suddenly a underscore in the tarball name
    MY_PNV=${PN/-/_}-${PV}

    require github [ user=Yubico release=${PV} suffix=tar.gz ]
    require setup-py [ import=setuptools blacklist=2 test=pytest work=${MY_PNV} ]
else
    require github [ user=Yubico release=${PV} suffix=tar.gz ]
    require setup-py [ import=setuptools blacklist=2 test=pytest ]
fi

SUMMARY="Python library and command line tool for configuring a YubiKey"

HOMEPAGE="https://developers.yubico.com/${PN}/"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}Release_Notes.html"

LICENCES="BSD-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/pyopenssl[>=0.15.1][python_abis:*(-)?]
        dev-python/pyusb[python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
    run:
        app-crypt/ccid
        dev-libs/libfido2[>=1.4.0-r1] [[ note = [ udev rules ] ]]
        sys-auth/yubikey-personalization
    test:
        dev-python/makefun[python_abis:*(-)?]
"

if ever at_least 5.0.0 ; then
    DEPENDENCIES+="
        build+run:
            dev-python/click[>=8.0][python_abis:*(-)?]
            dev-python/cryptography[>=3.0][python_abis:*(-)?]
            dev-python/fido2[>=1.0][python_abis:*(-)?]
            dev-python/keyring[>=23.4][python_abis:*(-)?]
            dev-python/pyscard[>=2.0][python_abis:*(-)?]
    "
else
    DEPENDENCIES+="
        build+run:
            dev-python/click[>=7.0][python_abis:*(-)?]
            dev-python/cryptography[>=2.1][python_abis:*(-)?]
            dev-python/fido2[>=0.9&<2.0][python_abis:*(-)?]
            dev-python/pyscard[>=1.9][python_abis:*(-)?]
    "
fi

install_one_multibuild() {
    setup-py_install_one_multibuild

    doman man/ykman.1
}

