# Copyright 2022 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]
require github [ user=mchehab tag="v${PV}" ]

SUMMARY="Tools to get Platform Reliability, Availability, and Serviceability (RAS) reports"
DESCRIPTION="
A replacement for edac-tools that collects all hardware error events reported by the Linux Kernel
from several sources (EDAC, MCE, PCI, ...) into one common framework.

rasdaemon is a user-space daemon that correlates the data about the RAS events and provides the
system administrator a comprehensive report. The errors are stored in a SQLite database, in order to
allow those data to be mined for information.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="platform: amd64 x86"

DEPENDENCIES="
    build+run:
        dev-db/sqlite:3
    run:
        dev-lang/perl:*
        dev-perl/DBI
        dev-perl/DBD-SQLite
        platform:amd64? ( sys-apps/dmidecode )
        platform:x86? ( sys-apps/dmidecode )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-all
    --with-sysconfdefdir=/etc/default
    --localstatedir=/var
)

src_install() {
    default

    # The package dumps its internal headers directly into /usr/arch/include - and it's not even
    # useful to have headers installed, since there's no library to link to.
    edo rm -r "${IMAGE}/usr/$(exhost --target)/include"

    # Systemd units are generated, but not installed
    insinto "/usr/$(exhost --target)/lib/systemd/system"
    doins misc/rasdaemon.service
    doins misc/ras-mc-ctl.service

    # Empty directory for drop-in user configuration for DIMM slot labels
    keepdir /etc/ras/dimm_labels.d
}

